Object = require "libs.classic"
require 'player'
require 'hardai'
require 'screen'

ww,wh = 600,600
love.window.setMode(ww,wh,{msaa=5})
love.window.setTitle("Schizzors")

function love.load()
	win_music = love.audio.newSource("assets/win.mp3")
	lose_music = love.audio.newSource("assets/lose.mp3")
	
	math.randomseed(os.time())
	screen = Screen()
	player = Player()
	ai = AI()

	screen:add(player)
	screen:add(ai)
end

function love.wheelmoved(x, y)
	screen:wheel(x,y)
end

function love.keypressed(key)
	screen:pressed(key)
end

function love.mousepressed(x, y, button)
	screen:mpressed(x, y,button)
end

function love.update(dt)
	screen:update(dt)
end

function love.draw()
	screen:draw()
end
