require 'libs.math'

Bullets = Object:extend()

function Bullets:new()
	self.img = love.graphics.newImage("assets/bullet.png")
	self.w,self.h = 4,4
	self.ox,self.oy = 2,2
	self.speed = 500
	self.limit = 30
	self.bullets = {}
end

function Bullets:add(x1,y1,x2,y2)
	if #self.bullets < self.limit then
		local angle = math.angle(x1,y1,x2,y2)
		table.insert(self.bullets,{
			start  = {x=x1,y=y1},
			finish = {x=x2,y=y2},
			x=x1,y=y1,
			angle=angle
		})
	end
end

function Bullets:remove(i)
	table.remove(self.bullets,i)
end

function Bullets:update(dt,cb)
	if love.keyboard.isDown("up") then
		self.speed = self.speed + 1
	end

	if love.keyboard.isDown("down") then
		self.speed = self.speed - 1
	end

	for i,b in ipairs(self.bullets) do
		if cb ~= nil then
			cb(i,b)
		end
		-- remove if out of bounds
		if b.x+self.w < 0 or b.x+self.w > ww then
			self:remove(i)
		end

		if b.y+self.h < 0 or b.y+self.h > wh then
			self:remove(i)
		end

		-- movement
		b.vx,b.vy = math.cos(b.angle),math.sin(b.angle)

		b.x = b.x + b.vx * dt * self.speed
		b.y = b.y + b.vy * dt * self.speed
	end
end

function Bullets:draw()
	for i,b in ipairs(self.bullets) do
		love.graphics.draw(self.img,b.x,b.y,b.angle,1,1,self.ox,self.oy)
	end
end