schizzors
=========

### Game idea
```
Players gets three forms: rock, paper and scissors.
Players can 
         shape-shift between those forms.
         move around and shoot.
Logic for who wins when shot is borrowed from rock-paper-scissors.
So, if rock shot scissors rock wins.
```
### Running

#### Unix
```
# Install love 10 with your package manager.
# Refer to love2d.org

#clone it
git clone https://gitlab.com/godwhoa/schizzors.git
cd schizzors

#run it
love .
```


#### Windows

```
Download https://gitlab.com/godwhoa/schizzors/raw/master/bin/schizzors_win32.zip
Unzip and run schizzors.exe
```