Logic = {}


local WinvLose = {
	paper='rock',
	rock = 'scissors',
	scissors='paper',
}

function Logic:bestform(form)
	for win,lose in pairs(WinvLose) do
		if form == lose then
			return win
		end
	end
end

function Logic:worstform(form)
	return WinvLose[form]
end

function Logic:win(form1,form2)
	if WinvLose[form1] == form2 then
		return true
	end
	return false
end