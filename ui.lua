End = {}
function End:load()
	self.font = love.graphics.newFont("assets/font.ttf", 40)
	love.graphics.setFont(self.font)
end

function End:draw()
	if screen.scores.ai > screen.scores.player then
		love.graphics.print({{219,35,35},"You Lose..."}, 500/3, 500/2)
	else
		love.graphics.print({{101,40,235},"You Win!"}, 500/3, 500/2)
	end
end