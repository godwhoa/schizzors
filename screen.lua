require 'ui'

Screen = Object:extend()

function Screen:new()
	self.scores = {player=0,ai=0}
	self.state = 'game'
	self.screens = {}
	self.collideable = {}
	End:load()
end

function Screen:add(screen)
	table.insert(self.screens,screen)
end

function Screen:wheel(x, y)
	for i,s in ipairs(self.screens) do
		if s.wheel ~= nil then
			s:wheel(x,y)
		end
	end
end

function Screen:pressed(key)
	for i,s in ipairs(self.screens) do
		if s.pressed ~= nil then
			s:pressed(key)
		end
	end
end

function Screen:mpressed(x, y,button)
	for i,s in ipairs(self.screens) do
		if s.mpressed ~= nil then
			if button == 1 then
				s:mpressed(x,y,'left')
			end
			if button == 2 then
				s:mpressed(x,y,'right')
			end
		end
	end
end

function Screen:update(dt)
	if self.state == 'game' then
		for i,s in ipairs(self.screens) do
			s:update(dt)
		end
	end
end

function Screen:draw()
	if self.state == 'menu' then
	end

	if self.state == 'game' then
		for i,s in ipairs(self.screens) do
			s:draw()
		end
	end

	if self.state == 'end' then
		End:draw()
	end
end