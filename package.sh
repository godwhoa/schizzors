#!/bin/bash

# Create .love
zip -9 -q -r bin/schizzors.love libs assets *.lua

#Package up for windows
cd bin
#Fuse .love with love.exe
unzip love-0.10.1-win32.zip
mv love-0.10.1-win32 schizzors_win32
cat schizzors_win32/love.exe schizzors.love > schizzors_win32/schizzors.exe

#Zip it up
zip -r schizzors_win32.zip schizzors_win32