require 'libs.logic'
require 'character'
require 'libs.math'

AI = Character:extend()
local ai_options = {'up','down','left','right'}
function randdir()
	return ai_options[love.math.random(#ai_options)]
end

function AI:new()
	AI.super.new(self)
	--pos
	self.x,self.y = 100,200
	--timers
	self.ktime,self.stime,self.mtime = 0,0,0
	--debug
	self.debug = false
	--add ai's update methods
	self.umethods = {'movement','kill','shapeshift','bull'}
	--probability of best shapeshift
	self.prob = 0.75
end

function AI:bull(dt)
	self.bullets:update(dt,function(i,b)
		local hit  = RectColl(ai.x,ai.y,ai.assets[ai.state.form].w,ai.assets[ai.state.form].h,
							  b.x,b.y,self.bullets.w,self.bullets.h)
		if hit and Logic:win(self.state.form,player.state.form) then
			print("Player Lost")
			screen.scores.ai = screen.scores.ai + 1
			screen.state = "end"
			lose_music:play()
		end
	end)
end


-- ai functions
function AI:movement(dt)
	self.mtime = self.mtime + dt
	if self.ktime >= 0.1 then
		self:move(randdir())
		self.mtime = 0
	end
end

function AI:kill(dt)
	self.ktime = self.ktime + dt
	if self.ktime >= 0.5 then
		self:shoot(player.x,player.y)
		self.ktime = 0
	end
end

function AI:shapeshift(dt)
	--change probablity of it chosing bestform based on distance
	local d = math.dist(player.x,player.y,self.x,self.y)
	self.prob = math.map(0,500,0,1,d)

	self.stime = self.stime + dt
	-- https://www.youtube.com/watch?v=MGTQWV1VfWk
	if self.stime >= 1.7 then
		if math.random() < self.prob then
			self.state.form = Logic:bestform(player.state.form)
		else
			self.state.form = Logic:worstform(player.state.form)
		end
		self.stime = 0
	end
end