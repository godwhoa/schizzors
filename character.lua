require 'libs.math'
require 'bullets'

Character = Object:extend()

function Character:new()
	-- for moving around
	self.x,self.y = 250,250
	self.vx,self.vy = 0,0
	self.displace = 0.1
	-- self.displace = 1
	self.speed = 270
	self.limit = 0.7
	--other update/draw methods
	self.umethods = {}
	self.dmethods = {}
	--bullets
	self.bullets = Bullets()
	-- assets
	self.assets = {}
	self:add_asset('rock','assets/rock.png',72, 54,2)
	self:add_asset('paper','assets/paper.png',49,64,2)
	self:add_asset('scissors','assets/scissors.png',47,80,1)
	-- states
	self.state = {}
	self.state.form = 'rock'
	self.state.emotion = 'happy'
	-- for being able to switch with scroll wheel
	self.state.wheel = 1
	self.assets.list = {'rock','paper','scissors'}
	--debug
	self.debug = false
end

function Character:add_asset(name,src,w,h,count)
	local img = love.graphics.newImage(src)
	local frames,states = {},{}
	for i=1,count do
		table.insert(frames,love.graphics.newQuad(w*(i-1), h*(i-1), w, h, img:getDimensions()))
	end
	if count == 2 then
		states = {happy=frames[1],ouch=frames[2]}
	else
		states = {happy=frames[1],ouch=frames[1]}
	end
	self.assets[name] = {w=w, h=h, img=img, states=states}
end

function Character:move(dir)
	-- up
	if dir == 'up' then
		self.vy = self.vy - self.displace
	end

	-- down
	if dir == 'down' then
		self.vy = self.vy + self.displace
	end
	
	-- left
	if dir == 'left' then
		self.vx = self.vx - self.displace
	end

	-- right
	if dir == 'right' then
		self.vx = self.vx + self.displace
	end
end

function Character:shoot(mx,my)
	self.bullets:add(self.x+self.assets[self.state.form].w/2,self.y+self.assets[self.state.form].h/2,mx,my)
end

function Character:changeform(dir)
    if dir == 'up' then
    	-- up
    	self.state.wheel = self.state.wheel + 1
    elseif dir == 'down' then
    	-- down
    	self.state.wheel = self.state.wheel - 1
    end 
    self.state.wheel = math.clamp(1,self.state.wheel,3)
    self.state.form = self.assets.list[self.state.wheel]
end

function Character:update(dt)
	--other update methods
	for i,m in ipairs(self.umethods) do
		self[m](self,dt)
	end

	-- reset if player goes off-screen
	--left wall
	if self.x+self.vx < 0 then
		self.x = 0
		self.vx = -(self.vx/2)*dt
	end
	-- right wall
	if self.x+self.vx > ww-self.assets[self.state.form].w then
		self.x = ww-self.assets[self.state.form].w
		self.vx = -(self.vx/2)*dt
	end

	--top wall
	if self.y+self.vy < 0 then
		self.y = 0
		self.vy = -(self.vy/2)*dt
	end

	-- down wall
	if self.y+self.vy > wh-self.assets[self.state.form].h then
		self.y = wh-self.assets[self.state.form].h
		self.vy = -(self.vy/2)*dt
	end
	self.vx = math.clamp(-self.limit, self.vx, self.limit)
	self.vy = math.clamp(-self.limit, self.vy, self.limit)
	-- integrate velocity	
	self.x = self.x + self.vx * dt * self.speed
	self.y = self.y + self.vy * dt * self.speed
end

function Character:draw()
	--other draw methods
	for i,m in ipairs(self.dmethods) do
		self[m](self)
	end
	self.bullets:draw()
	local img = self.assets[self.state.form].img
	local quad = self.assets[self.state.form].states[self.state.emotion]
	love.graphics.draw(img, quad, self.x, self.y)
	if self.debug then
		love.graphics.print(string.format("FPS: %d x: %d y: %d",love.timer.getFPS(),self.x, self.y), 10, 10)
		love.graphics.print(string.format("Bullet speed:%f",self.bullets.speed), ww/2, 25)
		love.graphics.print(string.format("vx: %f vy: %f",self.vx, self.vy), 10, 25)
		love.graphics.rectangle("line", self.x, self.y, self.assets[self.state.form].w, self.assets[self.state.form].h)
	end
end