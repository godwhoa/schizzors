require 'libs.math'
require 'character'

Player = Character:extend()

function Player:new()
	Player.super.new(self)
	--crosshair
	self.chimg = love.graphics.newImage('assets/crosshair.png')
	--debug
	self.debug = false
	--other update/draw methods
	self.umethods = {'key','collision','bull'}
	self.dmethods = {'crosshair'}
end

function Player:crosshair()
	local mx,my = love.mouse.getPosition()
	love.graphics.draw(self.chimg,mx-14,my-14)
end

function Player:bull(dt)
	self.bullets:update(dt,function(i,b)
		local hit  = RectColl(ai.x,ai.y,ai.assets[ai.state.form].w,ai.assets[ai.state.form].h,
							  b.x,b.y,self.bullets.w,self.bullets.h)
		if hit and Logic:win(self.state.form,ai.state.form) then
			print("Player wins")
			screen.scores.player = screen.scores.player + 1
			screen.state = "end"
			win_music:play()
		end
	end)
end

function Player:collision(dt)
	-- check if they over-lap
	local hit  = RectColl(self.x,self.y,self.assets[self.state.form].w,self.assets[self.state.form].h,
						ai.x,ai.y,ai.assets[ai.state.form].w,ai.assets[ai.state.form].h)
	local side = RectSides(self.x,self.y,self.assets[self.state.form].w,self.assets[self.state.form].h,
						ai.x,ai.y,ai.assets[ai.state.form].w,ai.assets[ai.state.form].h)
	if hit then
		if side == 'left' then
			self.x = ai.x-self.assets[self.state.form].w
			self.vx = -(self.vx/2)*dt
			ai.vx = -(ai.vx/2)*dt
		end
		if side == 'right' then
			self.x = ai.x+ai.assets[ai.state.form].w
			self.vx = -(self.vx/2)*dt
			ai.vx = -(ai.vx/2)*dt
		end
		if side == 'top' then
			self.y = ai.y-self.assets[self.state.form].h
			self.vy = -(self.vy/2)*dt
			ai.vy = -(ai.vy/2)*dt
		end
		if side == 'bottom' then
			self.y = ai.y+ai.assets[ai.state.form].h
			self.vy = -(self.vy/2)*dt
			ai.vy = -(ai.vy/2)*dt
		end
	end
end

function Player:key()
	-- up
	if love.keyboard.isDown("w") then
		self:move('up')
	end

	-- left
	if love.keyboard.isDown("a") then
		self:move('left')
	end

	-- down
	if love.keyboard.isDown("s") then
		self:move('down')
	end

	-- right
	if love.keyboard.isDown("d") then
		self:move('right')
	end
end

function Player:mpressed(mx,my,button)
	if button == 'left' then
		self:shoot(mx,my)
	end
end

function Player:pressed(key)
	if key == 'tab' then
		self.debug = not self.debug
	end
end

function Player:wheel(x,y)
    if y > 0 then
    	-- up
    	self:changeform('up')
    elseif y < 0 then
    	-- down
    	self:changeform('down')
    end 
end